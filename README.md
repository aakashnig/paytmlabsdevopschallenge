
Devops Challenge: 

Answer the following with as much detail as you are comfortable, and email your answers to the contact who pointed you here.

- You are free to assume any technique/technology you think relevant can be used to solve the given problem.
- State all assumptions in describing your solution

## Q1: You have been given 1000 nodes to bootstrap, install hadoop/elasticsearch/cassandra (whatever distributed software you'refamiliar with) and test/check. assume all 1000 nodes have been wired etc. How would you go about it?

+ We will use Ansible for bootstrapping nodes. We can modify the following Ansbile scripts, as per PayTMLab's requirement. The scripts are located in the directory (SolutiontoProblem1), within this directory. 
1. [ElasticSearch][idansible]
2. [Cassandra][idcassandra]
3. [Hadoop][idhadoop]

+ We need to provide list of hosts/ or their DNS name to the inventory file of each of the above Ansible project. 
+ We also need to make sure that the machine executing scripts, has the SSH keys to remotely access these machines. 

Since, there no centralized control in Ansible (like Puppet/Chef), same scripts can be used to install, and configure these systems from 1000 nodes to 10000 nodes. 

1. The only bottlneck is the network traffic (SSH traffic), machine executing these scripts can handle. 
2. To scale it up further( >1000), we can loadbalance the machine executing scripts. 
3. In such case, no of load balancers required = Total Expected SSH Traffic / SSH traffic one controller machine can handle by itself. 


## Q2: How would you go about installing OS on these nodes ? How much time would it take ? (what if we had 10000 nodes)

#### DNS, DHCP, PXE, Kickstart and Ansible based solution. 

1. Establish a DHCP server that can support the number of clients (VLANs and such)
2. Create an HTTP server that can host the installation
3. Create a kickstart script for the installation. The kickstart can be based off the ks-anaconda.cfg file created when the OS is installed for the first time in the root home directory (normally located at /root/)
4. Integrate the kickstart script and HTTP server with a TFTP server, to support PXE booting.
5. Reboot target machines and execute the Kickstart script. 
6. LDAP initial boot a box from Kickstart or equivalent deploying an appropriate basic OS image
7. Ensure the post-image-install script install Ansible on the OS.
8. Install distributed Software using the scripts from Question 1 above.  

#### To scale this up, we need to ensure scalability of  controllers (DNS, HTTP Servers.)  
1. Load-Balance DNS and HTTP servers, w.r.t to expected traffic. 
2. In such case, no of load balancers = Expected DNS, HTTP traffic of N nodes/ Traffic that can be handled by one Node. 


## Q3: How would you implement a system that distributes a file to 1000 nodes ? (Any changes if 10000 nodes ?)

+ Perl,Python script to SCP file over the centralized server to other servers. 
+ First Send the file to first X servers. 
+ As soon as these X Servers received the file (let's say at time t), they can start distributing the file too, so after approx. + time 2*t already X^2 servers have the file instead of 2*X in the original solution. 
+ After time 3*t already X^3 Servers have got the file.
+ The file can be further divided in chunks, so that a server can start redistributing it before it has received the whole file. (something like Torrent.)

## Q4: How would you prevent unauthorized access to the cluster resources ? (Assume cluster service does not have intrinsic authentication / authorization built in)

We will prevent un-authorized access to cluster resources, by implemnting following mechaninsms.

+ SSH based authentication. 
+ Restrict no of open ports. Open only ports that are required for Operations (SSH etc.)
+ Security Groups config.  
+ Virtual Private Cloud Configuration. (Set up all the cluster resources in one VPC, restric access to this VPC by outside world)
+ Raise Alerts/or Alarms for un-expected traffic, Anonymous traffic. 

[idansible]: https://bitbucket.org/aakashnig/paytmlabsdevopschallenge/src/94a57179befe8f465bc260dcefd528471253aa0b/SolutiontoProblem1/ansible__cassandra/?at=master  "Ansible"
[idcassandra]: https://bitbucket.org/aakashnig/paytmlabsdevopschallenge/src/94a57179befe8f465bc260dcefd528471253aa0b/SolutiontoProblem1/ansible__elasticsearch/?at=master  "Cassandra"
[idhadoop]: https://bitbucket.org/aakashnig/paytmlabsdevopschallenge/src/94a57179befe8f465bc260dcefd528471253aa0b/SolutiontoProblem1/ansible__hadoop/?at=master






